import React from 'react';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import storage from '../../storage/storage';
import Input from '@mui/material/Input';

export default class input extends React.Component {
    constructor(props) {
        super(props);

        this.inputRef = React.createRef();

        document.addEventListener('focusOn', () => {
            this.inputRef.current.focus();
        })

        this.state = {
            inputValue: '',
            ...storage.plugins.main
        }
    }

    sendMessage(text) {
        if (text === '') {
            return
        }
        this.props.messagePush({
            text,
            user: this.state.currentUser,

            id: (new Date()).getTime()
        });
        this.state.inputValue = '';
        this.inputRef.current.value = '';
        window.scrollTo(0, 200);

        document.dispatchEvent(new Event('focusOn'))
    }

    inputHandler(value) {
        this.state.inputValue = value;
        this.setState(this.state);
    }

    render() {
        return (
            <div className="input-container">
                <div className="item-container">
                    <Input ref={this.inputRef} type="text" value={this.state.inputValue} placeholder="tab your message here" className="message-input"
                           onInput={(event) => {
                               this.inputHandler(event.target.value, event);
                           }}

                           onKeyPress={(event) => {
                               if (event.key === 'Enter') {
                                   this.sendMessage(event.target.value, event)
                               }
                           }}/>
                    <Button variant="contained" sx={{flexGrow: 1}} endIcon={<SendIcon/>} className="Button"
                            onClick={() => {
                                this.sendMessage(this.state.inputValue);
                            }}></Button>
                </div>
            </div>
        )
    }
}
