import {createSlice} from '@reduxjs/toolkit'

export const users = createSlice({
    name: 'users',
    initialState: {
        users: [
            {
                name: 'Alex',
                isOnline: true,
            }
        ]
    },
    reducers: {
        signUpUser: (state, userName) => {
            const user = {
                name: userName,
                id: (new Date()).getTime(),
                isOnline: true,
            };

            state.users.push(user)
        },
        // setUserState
    },
});

export default users.reducer
