import { configureStore } from '@reduxjs/toolkit';
import rooms from './Room/rooms';
import users from './User/users';


export default configureStore({
    reducer: {
        rooms,
        users
    },
})
