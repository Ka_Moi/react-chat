import React from 'react';

export default class Profile extends React.Component {


    render() {
        return (
                <div className='flex profile'>
                    <img className="profile-img" src="/asset/pictures/profile-icon-placeholder.jpg" alt="icon"/>
                    <div>
                        <div className="profile-name">Profile name</div>
                        <div className="flex profile-status">
                            <button className="change-status-btn">status</button>
                        </div>
                    </div>
                </div>
        )
    }
}
