import React from 'react';

export default class messenger extends React.Component{
    render() {
        const messages = this.props.messages.map((message, key) => {
            const className = message.user.id === this.props.currentUser.id ? "user-message message" : "participant-message message";
            return  <div
                className={className} key={key}>
                <b>{message.user.name}</b>{message.text}
                <span>
                    <a className={"delete-icon"} href='#' onClick={() => {
                        const messageId = message.id
                        this.props.deleteMessage(messageId)
                    }}>x</a>
                </span>
            </div>
        });
        return (
            <div>
                {messages}
            </div>
        )
    }
}
