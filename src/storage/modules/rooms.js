export default {

    newRoomInputValue: '',

    rooms: [
        {
            name: 'Greetings',
            image: 'placeholder.png',
            users: [],
            id: 1,
            messages: [
                {
                    text: 'Hello',
                    user: {
                        name: 'chat bot',
                        isOnline: true,
                        id: 2
                    }
                }
            ]
        },
        {
            name: ' Dead Poets Society',
            image: 'placeholder.png',
            users: [
                {
                    name: 'John Keating',
                    image: 'placeholder.svg',
                    room: 'Society of Dead Poets',
                    isOnline: true,
                    id: 2,
                    status: `Status`
                },
                {
                    name: 'Neil Perry',
                    image: 'placeholder.svg',
                    room: 'Society of Dead Poets',
                    isOnline: false,
                    id: 3,
                    status: `Status`
                },
                {
                    name: 'Todd Anderson',
                    image: 'placeholder.svg',
                    room: 'Society of Dead Poets',
                    isOnline: true,
                    id: 4,
                    status: `Status`
                },
                {
                    name: 'Charlie Dalton',
                    image: 'placeholder.svg',
                    room: 'Society of Dead Poets',
                    isOnline: true,
                    id: 5,
                    status: `Status`
                },
                {
                    name: 'Knox Overstreet',
                    image: 'placeholder.svg',
                    room: 'Society of Dead Poets',
                    isOnline: false,
                    id: 6,
                    status: `Status`
                }
            ],
            id: 2,
            messages: []
        },
        {
            name: 'Fighting Club',
            image: 'placeholder.png',
            users: [
                {
                    name: 'Tyler Durden',
                    image: 'placeholder.svg',
                    room: 'Fighting Club',
                    isOnline: true,
                    id: 2,
                    status: `Status`
                },
                {
                    name: 'Ricky',
                    image: 'placeholder.svg',
                    room: 'Fighting Club',
                    isOnline: true,
                    id: 3,
                    status: `Status`
                },
                {
                    name: 'Angel Face',
                    image: 'placeholder.svg',
                    room: 'Fighting Club',
                    isOnline: false,
                    id: 4,
                    status: `Status`
                }
            ],
            id: 3,
            messages: []
        }
    ]
}
