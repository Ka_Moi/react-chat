export default {
    currentRoom: null,

    currentUser: {
        name: '',
        image: 'placeholder.svg',
        isOnline: true,
        id: 1
    }
}
