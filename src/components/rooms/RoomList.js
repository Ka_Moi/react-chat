import React from 'react';
import Room from './Room';
import Profile from './Profile';
import storage from '../../storage/storage';
import Input from '@mui/material/Input';
import Button from '@mui/material/Button';


export default class RoomList extends React.Component {
    constructor(props) {
        super(props);

        document.dispatchEvent(new Event('focusOn'))

        this.deleteRoom = this.deleteRoom.bind(this);

        this.state = storage.plugins.rooms;
    }

    createRoom(room) {
        if (this.state.newRoomInputValue === '') {
            return
        }
        room.id = (new Date()).getTime();
        this.state.rooms.push(room);
        this.state.newRoomInputValue = '';
        this.setState(this.state);
        storage.updateStorage()
    }

    inputHandler(value) {
        this.state.newRoomInputValue = value;
        this.setState(this.state);
        storage.updateStorage()
    }

    deleteRoom(roomId) {
        const roomIndex = this.state.rooms.findIndex(room => room.id === roomId);
        this.state.rooms.splice(roomIndex, 1);
        this.setState(this.state);
        storage.updateStorage()
    };

    render() {
        const roomList = this.state.rooms.map((room, key) => {
            const isActive = this.props.currentRoom.id === room.id;
            return <Room key={key} room={room} deleteRoom={this.deleteRoom} changeRoom={this.props.changeRoom}
                         isActive={isActive}/>
        });

        return (
            <div className='container-rooms border'>
                <div className="item-container">
                    <Profile/>
                    <h4 className="mg-pd">Create new room</h4>
                    <div className='create-room-input'>
                        <Input value={this.state.newRoomInputValue} type="text" placeholder="enter new room name" className="input-room-name" onInput={(event) => {
                            this.inputHandler(event.target.value)
                        }}/>
                        <Button variant={"contained"} className="add-room-btn" onClick={() => this.createRoom({
                            name: this.state.newRoomInputValue,
                            images: 'placeholder.png',
                            users: [],
                            messages: []
                        })}>add room</Button>
                    </div>
                    <h4 className="mg-pd">Rooms</h4>
                    <ul>{roomList}</ul>
                </div>
            </div>
        )
    }
}
