import main from './modules/main';
import users from './modules/users';
import rooms from './modules/rooms';
import serverData from "./serverData";

const storage = window.localStorage;

export default {
    plugins: {main, users, rooms},


    initSocketConnection() {
        serverData.readFile('./serverData')
    },

    updateStorage() {
        storage.setItem('stored', JSON.stringify(this.plugins));
    },

    parseData() {
        const storedData = storage.getItem('stored');
        if (!storedData) {
            return
        }
        this.plugins = JSON.parse(storedData)
    },
}
