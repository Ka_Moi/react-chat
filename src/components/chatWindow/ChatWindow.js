import React from 'react';
import Messenger from './Messenger';
import Input from './Input';

export default class ChatWindow extends React.Component {
    render() {
        return (
            <div className="chat-window-container">
                <div className="chat-window-upper"></div>
                <div className="chat-window-messenger">
                    <Messenger messages={this.props.currentRoom.messages} currentUser={this.props.currentUser} deleteMessage={this.props.deleteMessage}/>
                </div>
                <div className="chat-window-input">
                    <Input messagePush={this.props.messagePush}/>
                </div>
            </div>
        )
    }
}
