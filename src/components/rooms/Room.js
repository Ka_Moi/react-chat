import React from 'react';
import DeleteIcon from "@mui/icons-material/Delete";

export default class room extends React.Component {
    render() {
        const classActive = this.props.isActive ? 'chat active' : 'chat inactive';
        return (
            <li>
                <div className={`${classActive} flex`} onClick={() => {
                    this.props.changeRoom(this.props.room);
                }}>
                    <div>{this.props.room.name}</div>
                    <a href="#" onClick={() => {
                        this.props.deleteRoom(this.props.room.id)
                    }}><DeleteIcon color="disabled" fontSize="small"/>
                    </a>
                </div>
            </li>
        )
    }
}
