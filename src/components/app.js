import React from 'react';
import ChatWindow from './chatWindow/ChatWindow';
import RoomList from './rooms/RoomList';
import UserList from "./users/UserList";
import storage from '../storage/storage';

export default class App extends React.Component {
    constructor(props) {
        super(props);

        storage.parseData();

        this.changeRoom = this.changeRoom.bind(this);
        this.messagePush = this.messagePush.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);

        this.state = storage.plugins


        if (!this.state.main.currentRoom) {
            const firstRoom = this.state.rooms.rooms[0];
            this.changeRoom(firstRoom, true);
        }
    }

    changeRoom(room, skipSetState) {
        this.state.main.currentRoom = room;
        this.state.rooms.rooms.forEach((roomItem) => {
            const userToDelete = roomItem.users.findIndex(user => user.id === this.state.main.currentUser.id);
            if (userToDelete !== -1) {
                roomItem.users.splice(userToDelete, 1)
            }
        });

        this.state.main.currentRoom.users.push(this.state.main.currentUser)
        document.dispatchEvent(new Event('focusOn'))


        if(!skipSetState) {
            this.setState(this.state);
            storage.updateStorage()
        }
    }

    messagePush(message) {
        this.state.main.currentRoom.messages.push(message);
        this.setState(this.state);
        storage.updateStorage()
    }

    deleteMessage(message) {
        const messageToDelete = this.state.main.currentRoom.messages.findIndex(message => message.id === message);
        this.state.main.currentRoom.messages.splice(messageToDelete, 1);
        this.setState(this.state);
        storage.updateStorage()
    }

    render() {
        return (
            <div className='content-container flex'>
                <RoomList changeRoom={this.changeRoom} currentRoom={this.state.main.currentRoom}/>
                <ChatWindow currentRoom={this.state.main.currentRoom} currentUser={this.state.main.currentUser} messagePush={this.messagePush} deleteMessage={this.deleteMessage}/>
                <UserList currentRoom={this.state.main.currentRoom}/>
            </div>
        )
    }
}
