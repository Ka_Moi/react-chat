import React from 'react';
import User from './User';
import storage from '../../storage/storage';
import Button from '@mui/material/Button';

export default class UserList extends React.Component {
    constructor(props) {
        super(props);

        this.deleteUser = this.deleteUser.bind(this)

        this.state = storage.plugins;
    }

    deleteUser(userId) {
        const userToDelete = this.state.main.currentRoom.users.findIndex(user => user.id === userId);
        this.state.main.currentRoom.users.splice(userToDelete, 1);
        this.setState(this.state);
        storage.updateStorage()
    }

    clearStorage() {
        window.localStorage.clear();
        document.location.reload();
    }

    render() {
        const userList = this.props.currentRoom.users.map((user, key) => {
            return <User key={key} user={user} deleteUser={this.deleteUser}/>
        });
        return (
            <div className='container-users border'>
                <div className="users-list">
                    <h4 className="mg-pd">Participants</h4>
                    <ul>
                        {userList}
                    </ul>
                </div>
                <div>
                    <Button variant={"contained"} className="clear-button" onClick={() => this.clearStorage()}>clear storage</Button>
                </div>
            </div>
        )
    }
}
