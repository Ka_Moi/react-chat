const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const fs = require('fs')

app.get('/', (req, res) => {
    req.send('<h1>Hello</h1>');
});

io.on('connection', socket => {
    socket.on('update data', data => {
        fs.writeFile('./serverData', JSON.stringify(data), error => {
            if (error) {
                console.log(error);
            }
        });

        io.emit('data update', data);
    })
});

server.listen(3000, () => {
    console.log('listening on 3000');
});
