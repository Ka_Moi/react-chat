import { createSlice } from '@reduxjs/toolkit'

export const rooms = createSlice({
    name: 'rooms',
    initialState: {
        rooms: [
            {
                name: 'School chat',
                image: 'placeholder.png',
                users: [],
                id: 1
            },
            {
                name: 'Love chat',
                image: 'placeholder.png',
                users: [],
                id: 2
            },
            {
                name: 'Offtopic',
                image: 'placeholder.png',
                users: [],
                id: 3
            }
        ]
    },
    reducers: {
        createRoom: (state, room) => {
            room.id = (new Date()).getTime();
            state.rooms.push(room)
        },
        enterRoom: (state, userId, roomId) => {
            state.rooms.forEach(room => {
                if (room.users.indexOf(userId) === -1) {
                    return;
                }
                room.users.splice(room.users.indexOf(userId), 1)
            });
            const room = state.rooms.find(item => item.id === roomId);
            room.users.push(userId)
        }
    },
});

export default rooms.reducer
